CKEDITOR.editorConfig = function( config ) {


    config.stylesSet =
        [
            /* Block Styles */

            // Each style is an object whose properties define how it is displayed
            // in the dropdown, as well as what it outputs as html into the editor
            // text area.
            { name : 'Paragraph'   , element : 'p' },
            { name : 'Fred 2'   , element : 'h2' },
            { name : 'Heading 3'   , element : 'h3' },
            { name : 'Heading 4'   , element : 'h4' },
            { name : 'Float Right', element : 'div', attributes : { 'style' : 'float:right;' } },
            { name : 'Float Left', element : 'div', attributes : { 'style' : 'float:left;' } },
            { name : 'Preformatted Text', element : 'pre' },
        ];

    var lite = config.lite = config.lite || {};
    config.extraPlugins = 'lite';


};